# GitWorkshop
### Workshop on the basic usage of git
### Short link for this is at [bit.ly/git-uw](http://bit.ly/git-uw)
#### Information to download git: https://www.linode.com/docs/development/version-control/how-to-install-git-on-linux-mac-and-windows/
#### Information to download VS Code: https://code.visualstudio.com/download
#### Useful cheat sheet: https://rogerdudler.github.io/git-guide/
#### To clone this project: `git clone https://git.uwaterloo.ca/fdeserre/git-workshop.git`
#### To make Merge Requests, email your username to `fdeserre@uwaterloo.ca`